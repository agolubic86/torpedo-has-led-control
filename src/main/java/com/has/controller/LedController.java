package com.has.controller;

import com.has.constants.LedStatus;
import com.has.constants.Room;
import com.has.domain.Color;
import com.has.domain.Led;
import com.has.services.LedManager;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by torpedo on 01.12.17..
 */

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class LedController {

    @Value("${build.version}")
    private String buildVersion;

    @Value("${build.timestamp}")
    private String buildTimestamp;

    @Autowired
    private LedManager ledManager;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "LED SERVICE. Version: " + buildVersion + "- Build@" + buildTimestamp;
    }

    @RequestMapping(value = "/led/{newLedId}/room/{roomId}/name/{name}", method = RequestMethod.POST)
    public Led registerNewLed(@PathVariable("roomId") final Integer roomId, @PathVariable("newLedId") final Integer ledId, @PathVariable("name") final String name) {
        Led led = new Led(ledId, Room.getByValue(roomId), name);
        return ledManager.registerLed(led);
    }

   @RequestMapping(value = "/led/{ledId}/room/{roomId}", method = RequestMethod.GET)
    public Led findLedById(@PathVariable("ledId") final Integer ledId, @PathVariable("roomId") final Integer roomId) {
        return ledManager.findLed(roomId, ledId);
    }

    @RequestMapping(value = "/led/{ledId}/room/{roomId}", method = RequestMethod.DELETE)
    public void deleteLedById(@PathVariable("ledId") final Integer ledId, @PathVariable("roomId") final Integer roomId) {
        ledManager.deleteLed(roomId, ledId);
    }

    @RequestMapping(value = "led/room/{roomId}", method = RequestMethod.GET)
    public List<Led> findLedByRoomId(@PathVariable("roomId") final Integer roomId) {
        Assert.notNull(Room.getByValue(roomId), "Invalid room id");
        return ledManager.findLedByRoomId(roomId);
    }

    @RequestMapping(value = "/led", method = RequestMethod.GET)
    public List<Led> fetchLedList() {
        return ledManager.fetchLeds();
    }

    @RequestMapping(value = "led/{ledId}/room/{roomId}/on", method = RequestMethod.PUT)
    public Led ledControlOn(@PathVariable("roomId") final Integer roomId, @PathVariable("ledId") final Integer ledId) {
        Assert.notNull(Room.getByValue(roomId), "Invalid room id");
        return ledManager.changeLedStatus(ledId, roomId, LedStatus.ON);
    }

    @RequestMapping(value = "led/{ledId}/room/{roomId}/off", method = RequestMethod.PUT)
    public Led ledControlOff(@PathVariable("roomId") final Integer roomId, @PathVariable("ledId") final Integer ledId) {
        Assert.notNull(Room.getByValue(roomId), "Invalid room id");
        return ledManager.changeLedStatus(ledId, roomId, LedStatus.OFF);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "led/{ledId}/room/{roomId}/color/{r}/{g}/{b}", method = RequestMethod.POST)
    public Led ledChangeColor(@PathVariable("roomId") final Integer roomId, @PathVariable("ledId") final Integer ledId,
                              @PathVariable("r") final int red, @PathVariable("g") final int green,
                              @PathVariable("b") final int blue) {

        Color ledColor = new Color(red, green, blue);
        return ledManager.changeLedColor(roomId, ledId, ledColor);
    }


}
