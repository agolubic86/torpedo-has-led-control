package com.has.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.JsonObject;
import com.has.constants.LedStatus;
import com.has.constants.Room;
import com.has.domain.Color;
import com.has.domain.Led;
import com.has.domain.LedControlError;
import com.has.mqtt.MqttUtility;
import com.has.mqttHelper.LedMqttTopicProperties;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.model.MappingException;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by torpedo on 30.01.18..
 */
@Service
public class LedManagerImpl implements LedManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(LedManagerImpl.class);

    public static final String LED_ID_PARAM = "ledId";
    public static final String ROOM_PARAM = "room";
    public static final String LED_COLOR_PARAM = "ledColor";
    public static final String STATUS_PARAM = "status";

    @Autowired
    private LedMqttTopicProperties topicProperties;

    @Autowired
    private MqttUtility mqttUtility;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Led registerLed(Led led) {
        try {
            LOGGER.info("Registering new LED LED={}", led);
            if (mongoTemplate.exists(Query.query(Criteria.where(LED_ID_PARAM).is(led.getLedId()).and(ROOM_PARAM).is(led.getRoom())), Led.class))
                throw new LedControlError(String.format("LED with id = %s in room = %s already registered.", led.getLedId(), led.getRoom().name()));

            mongoTemplate.insert(led);

            LOGGER.info("Registering new LED LED={} - SUCCESSFUL", led);

            return led;
        }catch (MongoException me) {
            throw new LedControlError("Error registering new LED.", me);
        }
    }

    @Override
    public List<Led> fetchLeds() {
        return mongoTemplate.findAll(Led.class);
    }



    @Override
    public Led findLed(Integer ledId, Integer roomId) {
        Room room = Room.getByValue(roomId);
        return mongoTemplate.findOne(Query.query(Criteria.where(LED_ID_PARAM).is(ledId).and(ROOM_PARAM).is(room)), Led.class);
    }

    @Override
    public List<Led> findLedByRoomId(Integer roomId) {
        return mongoTemplate.find(Query.query(Criteria.where(ROOM_PARAM).is(Room.getByValue(roomId))), Led.class);
    }

    @Override
    public Led changeLedStatus(Integer ledId, Integer roomId, LedStatus status) {
        Map<String, Object> params = setDefaultParams(roomId, ledId);
        params.put(STATUS_PARAM, status);

        return updateObjectAndSendMessage(params);
    }

    @Override
    public Led changeLedColor(Integer roomId, Integer ledId, Color ledColor) {
        Map<String, Object> params = setDefaultParams(roomId, ledId);
        params.put(LED_COLOR_PARAM, ledColor);

        return updateObjectAndSendMessage(params);
    }

    @Override
    public void deleteLed(final Integer roomId, final Integer ledId) {
        Room room = Room.getByValue(roomId);
        try {
            mongoTemplate.remove(Query.query(Criteria.where(LED_ID_PARAM).is(ledId).and(ROOM_PARAM).is(room)));
        }catch (MongoException me) {
            String msg = String.format("Error deleting LED with id=%s in room=%s", ledId, room);
            throw new LedControlError(msg, me);
        }catch (MappingException mpe) {
            String msg = String.format("Error deleting LED with id=%s in room=%s", ledId, room);
            throw new LedControlError(msg, mpe);
        }
    }

    private Map<String, Object> setDefaultParams(Integer roomId, Integer ledId) {
        Map<String, Object> params = new HashMap<>();
        params.put(LED_ID_PARAM, ledId);
        params.put(ROOM_PARAM, Room.getByValue(roomId));
        return params;
    }

    private Led updateObjectAndSendMessage(Map<String, Object> params) {
        Led led = updateLed(params);
        sendLedControlMessage(led);

        return led;
    }

    private Led updateLed(Map<String, Object> params) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where(LED_ID_PARAM).is(params.get(LED_ID_PARAM)).and(ROOM_PARAM).is(params.get(ROOM_PARAM)));

            Update update = new Update();
            if (params.containsKey(STATUS_PARAM))
                update.set(STATUS_PARAM, params.get(STATUS_PARAM));

            if (params.containsKey(LED_COLOR_PARAM))
                update.set(LED_COLOR_PARAM, params.get(LED_COLOR_PARAM));

            FindAndModifyOptions options = new FindAndModifyOptions();
            options.upsert(false).returnNew(true);

            Led led = mongoTemplate.findAndModify(query, update, options, Led.class);
            if (led == null) {
                throw new LedControlError("No LED found to update.");
            }

            return led;
        }catch (MongoException e) {
            throw new LedControlError("Error updating status.", e);
        }

    }

    private void sendLedControlMessage(Led led){

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
            mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
            JsonObject payload = new JsonObject();
            String unescapedJson = mapper.writeValueAsString(led);
            payload.addProperty("led", unescapedJson);

            mqttUtility.publish(topicProperties.getPublish(), unescapedJson);
        }catch (JsonProcessingException e) {
            throw new LedControlError("Error mapping object as JSON.", e);
        }

    }
}
