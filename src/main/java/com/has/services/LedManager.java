package com.has.services;


import com.has.constants.LedStatus;
import com.has.domain.Color;
import com.has.domain.Led;

import java.util.List;

/**
 * Created by torpedo on 30.01.18..
 */
public interface LedManager {

    Led changeLedStatus(Integer ledId, Integer roomId, LedStatus status);

    Led findLed(Integer ledId, Integer roomId);

    List<Led> fetchLeds();

    Led registerLed(Led led);

    List<Led> findLedByRoomId(Integer roomId);

    Led changeLedColor(Integer roomId, Integer ledId, Color ledColor);

    void deleteLed(Integer roomId, Integer ledId);
}
