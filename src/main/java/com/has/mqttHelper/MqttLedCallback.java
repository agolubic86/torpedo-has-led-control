package com.has.mqttHelper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.has.domain.Led;
import com.has.mqtt.CustomMqttCallback;
import com.has.services.LedManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by torpedo on 18.02.18..
 *
 */
@Component
public class MqttLedCallback extends CustomMqttCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(MqttLedCallback.class);

    @Autowired
    private LedManager ledManager;

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {

        try {
            LOGGER.info("Mapping MQTT message to object. Message={}", message);
            ObjectMapper mapper = new ObjectMapper();
            Led led = mapper.readValue(message.getPayload(), Led.class);
            ledManager.registerLed(led);
        }catch (JsonMappingException jme) {
            LOGGER.error("Error mapping message={} to object.", message, jme);
        }catch (JsonParseException jpe) {
            LOGGER.error("Error parsing message={}", message, jpe);
        }catch (Exception e) {
            LOGGER.error("Error with MQTT callback.", e);
        }
    }
}
