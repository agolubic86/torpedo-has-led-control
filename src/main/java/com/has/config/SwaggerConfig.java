package com.has.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by torpedo on 02.12.17..
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket ledApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                //.groupName("led-control-api")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.has"))
                .paths(ledPaths())
                .build();
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Torpedo Home Automation System API")
                .description("REST API for IOT home automation system controlling LED lights.")
                //.termsOfServiceUrl("http://springfox.io")
                .contact(new Contact("torpedo", "torpedo.com", "agolubic86@gmail.com"))
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")
                .version("2.0")
                .build();
    }

    private Predicate<String> ledPaths() {
        return or(
                regex("/.*")
        );
    }
}
