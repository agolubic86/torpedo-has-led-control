package com.has;

import com.has.mqtt.MqttUtility;
import com.has.mqttHelper.LedMqttTopicProperties;
import com.has.mqttHelper.MqttLedCallback;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableDiscoveryClient
@PropertySource({"classpath:/mqtt.properties", "classpath:/mongodb.properties", "classpath:/application.properties"})
public class TorpedoHasLedControlApplication {

	@Bean
	public MqttLedCallback mqttLedCallback() {
		return new MqttLedCallback();
	}

	@Bean
	public MqttUtility mqttUtility() {
		return new MqttUtility(mqttLedCallback());
	}

	public static void main(String[] args) {

		System.setProperty("spring.config.name", "led-server");
		ApplicationContext context = SpringApplication.run(TorpedoHasLedControlApplication.class, args);

		MqttUtility mqttUtility = context.getBean(MqttUtility.class);
		LedMqttTopicProperties topicProperties = context.getBean(LedMqttTopicProperties.class);
		mqttUtility.subscribe(topicProperties.getSubscribe());
	}
}

