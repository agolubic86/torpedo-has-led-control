package com.has.domain;

/**
 * Created by torpedo on 01.02.18..
 */
public class LedControlError extends RuntimeException {

    public LedControlError(String errorMessage) {
        super(errorMessage);
    }

    public LedControlError(String errorMessage, Throwable th) {
        super(errorMessage, th);
    }
}
