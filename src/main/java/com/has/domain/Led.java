package com.has.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.has.constants.LedStatus;
import com.has.constants.Room;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * Created by torpedo on 01.12.17..
 */

@JsonRootName(value = "led")
@Document(collection = "led")
public class Led {

    public Led() {
        this.status = LedStatus.OFF;
        this.ledColor = new Color();
    }



    @Id
    @JsonIgnore
    private String id;
    @NotNull
    private Integer ledId;
    @JsonIgnore
    private String name;
    @JsonIgnore
    private LedStatus status;
    @NotNull
    @JsonIgnore
    private Room room;
    private Color ledColor;

    public Led(Integer ledId, Room room) {
        this.ledId = ledId;
        this.room = room;
        this.status = LedStatus.OFF;
        this.ledColor = new Color();
    }

    public Led(Integer ledId, Room room, String name) {
        this.ledId = ledId;
        this.room = room;
        this.name = name;
        this.status = LedStatus.OFF;
        this.ledColor = new Color();
    }

    public Led(Integer ledId, Room room, String name, LedStatus status) {
        this.ledId = ledId;
        this.room = room;
        this.name = name;
        this.status = status;
        this.ledColor = new Color();
    }

    public Led(Integer ledId, Room room, LedStatus status, String name, Color ledColor) {
        this.ledId = ledId;
        this.room = room;
        this.name = name;
        this.status = status;
        this.ledColor = ledColor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getLedId() {
        return ledId;
    }

    public void setLedId(Integer ledId) {
        this.ledId = ledId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LedStatus getStatus() {
        return status;
    }

    public void setStatus(LedStatus status) {
        this.status = status;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Color getLedColor() {
        return ledColor;
    }

    public void setLedColor(Color ledColor) {
        this.ledColor = ledColor;
    }

    @Override
    public String toString() {
        return "Led: id=" + id
                + " status=" + status.name();
    }
}
