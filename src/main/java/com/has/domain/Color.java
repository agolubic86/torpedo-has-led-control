package com.has.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * Created by torpedo on 24.02.18.
 */
public class Color {

    public Color() {
        this.r = Integer.valueOf(0);
        this.g = Integer.valueOf(0);
        this.b = Integer.valueOf(0);
        this.alpha = Double.valueOf(0);
    }

    public Color(int r, int g, int b, double alpha){
        this.r = r;
        this.g = g;
        this.b = b;
        this.alpha = alpha;
    }

    public Color(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
        alpha = Double.valueOf(1);
    }

    @JsonSerialize(using = ToStringSerializer.class)
    private int r;
    @JsonSerialize(using = ToStringSerializer.class)
    private int g;
    @JsonSerialize(using = ToStringSerializer.class)
    private int b;
    @JsonSerialize(using = ToStringSerializer.class)
    private double alpha;

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }
}
