package com.has.constants;

/**
 * Created by torpedo on 30.01.18..
 */
public enum LedStatus {
    ON(1),
    OFF(0);

    private final int statusValue;

    LedStatus(int statusValue) { this.statusValue = statusValue; }
    public int getValue() { return statusValue; }
}
